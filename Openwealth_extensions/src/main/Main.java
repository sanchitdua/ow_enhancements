package main;

import java.util.*;
import java.util.regex.*;
import java.io.*;

public class Main {

	public static void main(String[] args) {
		BufferedReader br = null;
		List<List<String>> input = new ArrayList<List<String>>();
		
		try {
 
			String sCurrentLine;
 
			br = new BufferedReader(new FileReader("C:\\Users\\user\\workspace\\Openwealth_extensions\\src\\OW_CONT_Duplicate.csv"));
			br = new BufferedReader(new FileReader("C:\\Users\\user\\workspace\\Openwealth_extensions\\src\\OW_CONT_Quotes.csv"));
			while ((sCurrentLine = br.readLine()) != null) {
				List<String> row = new ArrayList<String>();
				for(String value: sCurrentLine.split(",")){
					row.add(value);
				}
				input.add(row);
//				System.out.println(sCurrentLine);
			}
 
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		
//		System.out.println("The input is: "+input);
		
		
		List<List<String>> output = new ArrayList<List<String>>();
		
//		output = Utility.removeDuplicates(input);
		output = Utility.removeQuotations(input);
		
		String REGEX ="(?<!^\\s*|,)\"\"(?!,\"\"|\\s*$)";
//		        "/(?:(?<=^\")|(?<=\",\")).*?(?:(?=\"\\s*$)|(?=\",\"))/g";
//		    private static final String INPUT =
//		        "dog dog dog doggie dogg";
		String INPUT = "\"testing\"\",\"tester\"";
//		testString.replaceAll("/(?:(?<=^\")|(?<=\",\")).*?(?:(?=\"\s*$)|(?=\",\"))/g", replacement)
//		System.out.println("the testString is : "+testString);
		
		
		System.out.println(INPUT);
		
		
		Pattern p = Pattern.compile(REGEX);
	       //  get a matcher object
	       Matcher m = p.matcher(INPUT);
	       int count = 0;
	       while(m.find()) {
	           count++;
	           System.out.println("Match number "
	                              + count);
	           System.out.println("start(): "
	                              + m.start());
	           System.out.println("end(): "
	                              + m.end());
	      }
		
		
		
		
		
		
		
		
		
		
	}

}
