package main;

import java.util.*;
import java.io.*;

public class Utility {
	public static List<List<String>> removeDuplicates(List<List<String>> dataList){
		List<List<String>> returnVal = new ArrayList<List<String>>();
		
		Map<String, List<String>> rowMap = new HashMap<String, List<String>>();
		
		// unique record and the complete row		Map<String, List<String>> rowMap = new HashMap<String, List<String>>();
		for(List<String> row: dataList){ // Iterate over each row
			// Let's say we have the unique records maintained based on the 10th column ie of index = 9
			rowMap.put(row.get(9), row);
			System.out.println("the column index 9 is : "+row.get(9));
		} // END for

		try {
			 
			String content = "";
 
			for(String key: rowMap.keySet()) {
				
				for(String value: rowMap.get(key)){
					content = content+value+",";
				}
				
				if(content.endsWith(","))
					content = content.substring(0, content.length()-1);
				
				content = content + "\n";
				
				
			}
			
			
			File file = new File("C:\\Users\\user\\workspace\\Openwealth_extensions\\src\\OW_CONT_Non_Duplicate.csv");
 
			// if file doesnt exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}
 
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(content);
			bw.close();
 
			System.out.println("Done");
 
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		for(String key: rowMap.keySet()) {
			returnVal.add(rowMap.get(key));
		}
		
		return returnVal;
		
	} // END public static List<List<String>> removeDuplicates(List<List<String>> dataList)
	
	
	public static List<List<String>> removeQuotations (List<List<String>> input) {
		 List<List<String>> returnVal = new ArrayList<List<String>>();
		 
		 for(List<String> row: input){
			 
			 List<String> new_row = new ArrayList<String>();
//			 int counter=0;
			 for(String value: row) {
				 if(value.contains("\"")){
					 value = value.replaceAll("\"", "");
					 new_row.add("\""+value+"\"");
				 } else
					 new_row.add(value);
//				 if(counter<15)
//					 System.out.println("the data is: "+value);
//				 else
//					 break;
//				 counter++;
			 }
			 returnVal.add(new_row);
		 } // END for
		 
		 try {
			 
				String content = "";
	 
				for(List<String> key: returnVal) {
					
					for(String value: key){
						content = content+value+",";
					}
					
					if(content.endsWith(","))
						content = content.substring(0, content.length()-1);
					
					content = content + "\n";
					
					
				}
				
				
				File file = new File("C:\\Users\\user\\workspace\\Openwealth_extensions\\src\\OW_CONT_Non_Quotes.csv");
	 
				// if file doesnt exists, then create it
				if (!file.exists()) {
					file.createNewFile();
				}
	 
				FileWriter fw = new FileWriter(file.getAbsoluteFile());
				BufferedWriter bw = new BufferedWriter(fw);
				bw.write(content);
				bw.close();
	 
				System.out.println("Done");
	 
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		 return returnVal;
	} // END public List<List<String>> removeQuotations (List<List<String>> input)
	
	
}
